package org.example.kelas;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class DataSiswa {
    public void Siswa(){

            ArrayList<Integer> listID = new ArrayList<Integer>();
            ArrayList<String> listName =  new ArrayList<String>();
            ArrayList<Integer> listNili = new ArrayList<Integer>();
            ArrayList<String> listGrade = new ArrayList<String>();
            String grade;

        while(true){
            Scanner input = new Scanner(System.in);
            System.out.println("===========MENU==========");
            System.out.println("1. Buat Object Siswa");
            System.out.println("2. Edit Data Siswa");
            System.out.println("3. Remove Object Siswa");
            System.out.println("4. Laporan Data Siswa");
            System.out.println("5. Exit");

            System.out.print("Masukan Menu : ");
            int value = input.nextInt();
            switch (value){
                case 1:
                    System.out.println("====INPUT DATA SISWA===");
                    System.out.print("Input ID : ");
                    final int Id = input.nextInt();
                    listID.add(Id);
                    System.out.print("Input Nama : ");
                    String name = input.next();
                    listName.add(name);
                    System.out.print("Input Nilai : ");
                    int nilai = input.nextInt();
                    listNili.add(nilai);
                    System.out.println("Data Berhasil Dimasukkan");

                    if(nilai <= 20){
                        grade = "E";
                        listGrade.add(grade);
                    }else if(nilai >= 21 || nilai <= 40){
                        grade = "D";
                        listGrade.add(grade);
                    }else if(nilai >= 41 || nilai <= 60){
                        
                    }
                    break;
                case 2:
                    System.out.println("Input ID : ");
                    int inputId = input.nextInt();

                    System.out.println("ID : " + listID.get(inputId-1));
                    System.out.println("NAME : " + listName.get(inputId-1));
                    System.out.println("NILAI SEBELUM DI RUBAH : " + listNili.get(inputId-1));
                    System.out.println("Input Nilai : ");
                    int ubahNilai = input.nextInt();
                    listNili.set(inputId-1, ubahNilai);

                    System.out.println("Nilai setelah dirubah : " + listNili.get(inputId-1));
                    break;
                case 3:
                    System.out.println("Masukan ID : ");
                    int removeID = input.nextInt();
                    listID.remove(removeID-1);
                    System.out.println("Data Berhasil Dihapus");
                    break;
                case 4:
                    System.out.println("Laporan Data Siswa");
                    System.out.println("=======================================");
                    System.out.println("|ID       Nama       Nilai       GRADE|");
                    System.out.println("=======================================");
                    int output = listID.size();
                    for (int i = 0; i < output ; i++) {
                        System.out.print(listID.get(i) + "\t");
                        System.out.print("\t" + listName.get(i));
                        System.out.println("\t" + listNili.get(i) + "\t|");
                    }
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan Salah");
            }
//        ArrayList<Integer> listId = new ArrayList<Integer>();
//        System.out.print("Input ID : \n");
//        while (input.hasNextInt()){
//            listId.add(input.nextInt());
//        }
//        Integer[] dataId = listId.toArray(new Integer[0]);
//        for(int i = 0; i < dataId.length; i++){
//            System.out.println(dataId[i]);
//        }
//
//        ArrayList<String> dNama = new ArrayList<String>();
//        input.hasNextInt();
//
//        ArrayList<Integer> dNilai = new ArrayList<Integer>();

//            static void addSiswa() {
//
//            }
        }

    }

//    static void addSiswa(){
//
//    }

    public static void main(String[] args) {
        DataSiswa dataSiswa = new DataSiswa();
        dataSiswa.Siswa();
    }
}

