package org.example.latihan;

import java.util.Scanner;

public class MusimBulan {
    public void showMusim(){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan bulan : ");
        String  bulan = input.nextLine();

        switch (bulan) {
            case "Desember":
            case "Januari":
            case "Februari":
                System.out.println(bulan + " adalah Musim Dingin");
                break;
            case "Maret":
            case "April":
            case "Mei":
                System.out.println(bulan + " adalah Musim Semi");
                break;
            case "Juni":
            case "Juli":
            case "Agustus":
                System.out.println(bulan + " adalah Musim Panas");
                break;
            case "September":
            case "Oktober":
            case "November":
                System.out.println(bulan + " adalah Musim Gugur ");
                break;
            default:
                System.out.println("Bulan yg anda Masukan Nyasar");
        }
    }

    public static void main(String[] args) {
        MusimBulan musimBulan = new MusimBulan();
        musimBulan.showMusim();
    }
}
