package org.example.latihan;

import java.util.Scanner;

public class HargaDiskon {
    public void showHarga() {
        Scanner input = new Scanner(System.in);

        System.out.print("Harga : Rp.");
        float harga = input.nextFloat();

        float Diskon = harga*10/100;
        System.out.println("Diskon : Rp." + Diskon);

        float HargaBayar = harga-Diskon;
        System.out.println("Harga Bayar : Rp." + HargaBayar);
    }

    public static void main(String[] args) {
        HargaDiskon hargaDiskon = new HargaDiskon();
        hargaDiskon.showHarga();
    }
}
