package org.example.latihan;

public class TagihanListrik {
    public void tagihanListrik() {
        String noPelanggan = "001";
        String name     = "Medula";
        String bulan    = "Juli";
        int pulsa     = 110;
        float tarif     = 250;
        float biaya     = 20000;

        float tagihan = (pulsa*tarif) + biaya;
        System.out.println("Nomor Pelanggan : " + noPelanggan);
        System.out.println("Nama Pelanggan : " + name);
        System.out.println("Bulan Tagihan : " + bulan);
        System.out.println("Banyak Pulsa Pemakaian : " + pulsa);
        System.out.println("Jumlah Tagihan Bulan " + bulan + "adalah sebesar Rp." + tagihan);
    }

    public static void main(String[] args) {
        TagihanListrik tagihanListrik = new TagihanListrik();
        tagihanListrik.tagihanListrik();
    }
}
