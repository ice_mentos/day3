package org.example.latihan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LaporanPenjualan {
    public void showLaporan() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(isr);

        String nameBrg[] = new String[5];
        int tabelJual = 0;
        int harga[] = new int[5];
        int jml[] = new int[5];
        int total[] = new int[5];
        int tabelBeli[] = new int[5];

        System.out.println("Masukan Bulan Penjualan : ");
        String bulan =input.readLine();

        System.out.println("Masukan Jumlah Data : ");
        int jmlData = Integer.parseInt(input.readLine());

        for (int i = 0; i < jmlData ; i++) {
            System.out.println("Nama Barang ke " + i + ":");
            nameBrg[i] = input.readLine();
            System.out.println("Jumlah : ");
            jml[i] = Integer.parseInt(input.readLine());
            System.out.println("Harga Rp. : ");
            harga[i] = Integer.parseInt(input.readLine());
            System.out.println("");
        }

        System.out.println("");
        System.out.println("Laporan Penjualan PT. NEXSFOT");
        System.out.println("Bulan : " + bulan);
        System.out.println("=============================================================================");
        System.out.println("NO      NAMA BARANG        JUMLAH           HARGA         TOTAL              ");
        System.out.println("=============================================================================");

        for (int j = 0; j < jmlData ; j++) {
            tabelBeli[j] = jml[j] * harga[j];
            total[j] += tabelBeli[j];
            tabelJual += total[j];
            System.out.println(j + "           " + nameBrg[j] + "          " + jml[j] + "             " +  harga[j] + "           " + total[j] +  "         ");
        }

        System.out.println("===============================================================");
        System.out.println("TOTAL BARANG   : " + jmlData);
        System.out.println("TOTAL PENJULAN : " + tabelJual);
    }

    public static void main(String[] args) throws IOException {
        LaporanPenjualan laporanPenjualan = new LaporanPenjualan();
        laporanPenjualan.showLaporan();
    }
}
