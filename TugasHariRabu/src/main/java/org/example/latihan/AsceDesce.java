package org.example.latihan;

import java.util.Arrays;
import java.util.Collections;

public class AsceDesce {
    public void showAsceDesce(){
        Integer[] data = {5, 1, 3, 2, 4};

        System.out.println("Data Asli : ");
        for (int i = 0; i < data.length ; i++) {
            System.out.print(data[i] + " ");
        }
        System.out.println();

        Arrays.sort(data);
        System.out.println("Pengurutan Ascending : ");
        for (int i = 0; i < data.length ; i++) {
            System.out.print(data[i] + " ");
        }
        System.out.println();

        Arrays.sort(data, Collections.reverseOrder());
        System.out.println("Pengurutan Descending : ");
        for (int i = 0; i < data.length ; i++) {
            System.out.print(data[i] + " ");
        }
    }
    public static void main(String[] args) {
        AsceDesce asceDesce = new AsceDesce();
        asceDesce.showAsceDesce();
    }
}
