package org.example.latihan;

import java.util.Scanner;

public class Saldo {
    public void showSaldo(){
        double bunga = 0.02;

        Scanner input =  new Scanner(System.in);

        System.out.print("Masukkan saldo anda : Rp. ");
        double saldo = input.nextDouble();

        System.out.println("Saldo Awal Rp. " + saldo);

        double saldoAkhir = (saldo*bunga)+saldo;
        System.out.println("Setelah berjalan satu bulan dengan pertimbangan bunga dan biaya admin," +
                "saldo baru Anda adalah Rp. " + saldoAkhir);
    }

    public static void main(String[] args) {
        Saldo saldo = new Saldo();
        saldo.showSaldo();
    }
}
