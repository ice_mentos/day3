package org.example.latihan;

import java.util.Scanner;

public class Restaurant {
    public void showRestaurant(){
        int nasGor = 5000;
        int nasSot = 7000;
        int gado_gado = 4500;
        int buburAyam = 4000;

        Scanner input = new Scanner(System.in);

        System.out.println("    Menu Resturant Mc'Cihuy     ");
        System.out.println("==================================");
        System.out.println("1. Nasi Goreng Informatika   Rp. " + nasGor + ",-");
        System.out.println("2. Nasi Soto Ayam Internet   Rp. " + nasSot + ",-");
        System.out.println("3. Gado-Gado Disket          RP. " + gado_gado + ",-");
        System.out.println("4. Bubut Ayam LAN            Rp. " + buburAyam + ",-");
        System.out.println("==================================");
        System.out.print("Masukan Pilihan Anda : ");
        int pilihan = input.nextInt();

        switch (pilihan) {
            case 1:
                System.out.println("Nasi Goreng Disket Rp. " + nasGor);
                break;
            case 2:
                System.out.println("Nasi Soto Ayam Internet Rp. " + nasSot);
                break;
            case 3:
                System.out.println("Gado-Gado Disket Rp. " + gado_gado);
                break;
            case 4:
                System.out.println("Bubur Ayam LAN Rp. " + buburAyam);
                break;
            default:
                System.out.println("Pilihan Tidak Tersedia");
        }
    }

    public static void main(String[] args) {
        Restaurant restaurant = new Restaurant();
        restaurant.showRestaurant();
    }
}
